<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function showCatgory(Request $request){
        $results = Category::all();
        // dd($results);
        return view('category.index',
         compact('results')
    );

    }
    public function create (){
        return view('category.form');
    }
    public function store(Request $request){
        // dd($request->all());
        $category = Category::create([
            'name' =>$request->name,
            'description' =>$request->description
        ]);
        return redirect('category');
    }

   public function edit($id){

       $category = Category::findOrFail($id);
    //    dd($category);
    return view('category.form', compact('category'));

   }

   public function update(Request $request,$id){

       $category = Category::find($id);
    // dd($request->all());
       $category->name = $request->name;
       $category->description = $request->description;
       $category->save();

       return redirect('category');

   }

   public function destroy($id){
    // dd($id);
    $category = Category::find($id);
    $category->delete();

    return redirect('category');

   }

}
