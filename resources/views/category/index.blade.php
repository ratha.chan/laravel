<!DOCTYPE html>
<html lang="en">
    {{-- @php
        dd($results);
    @endphp --}}
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th>
            <a href="{{url('category/create')}}" class="btn btn-link">Create category</a>
            </th>
          </tr>
        </thead>
        <tbody>
            @foreach ($results as $result)
                <tr>
                    <td>{{$result->id}}</td>
                    <td>{{$result->name}}</td>
                    <td>{{$result->description}}</td>
                    <td>
                        <a href="{{url('category/'.$result->id.'/edit')}}">edit</a>
                    </td>
                    <td>
                        <form method="POST" action="{{url('category/'.$result->id)}}">
                            <a href="{{url('category/'.$result->id)}}">delete</a>
                        </form>

                    </td>
                </tr>

            @endforeach


        </tbody>
      </table>

</body>
</html>

