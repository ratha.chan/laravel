@php
    $id          = isset($category) ? $category->id : null;
    $name        = isset($category) ? $category->name : null;
    $description = isset($category) ? $category->description : null;
@endphp

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
   <div class="container center">
        <div class="row">
            <div class="col-md-4 col-md-offset-6">
                <h1 class="center">Create Category</h1><br><br>
                <form method="POST" action="{{url('/category/'.$id)}}">
                    @csrf
                    @if ($id)

                    @method('PUT')

                    @endif
                    <div class="form-group">
                      <label>Name</label>
                     <input type="text" class="form-control" name="name" value="{{$name}}">
                    </div>
                    <div class="form-group">
                      <label>Descrption</label>
                     <textarea name="description" class="form-control">{{$description}}</textarea>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>


            </div>

        </div>
   </div>

</body>
</html>
